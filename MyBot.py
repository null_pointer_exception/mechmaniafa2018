# keep these three import statements
import game_API
import fileinput
import json

# your import statements here
import random 
first_line = True # DO NOT REMOVE

# global variables or other functions can go here
stances = ["Rock", "Paper", "Scissors"]
bullies = [4, 8, 11, 15, 17, 21]

def get_winning_stance(stance):
    if stance == "Rock":
        return "Paper"
    elif stance == "Paper":
        return "Scissors"
    elif stance == "Scissors":
        return "Rock"
    else:
        return "Rock"

def torps(s):
    if s == "Rock":
        return "R"
    elif s == "Paper":
        return "P"
    elif s == "Scissors":
        return "S"
    else:
        return ""

def fromrps(s):
    if s == "R":
        return "Rock"
    elif s == "P":
        return "Paper"
    elif s == "S":
        return "Scissors"
    else:
        return ""

c="""from collections import defaultdict
import operator
import random


if inp == "":
	score  = {'RR': 0, 'PP': 0, 'SS': 0, \
	          'PR': 1, 'RS': 1, 'SP': 1, \
	          'RP': -1, 'SR': -1, 'PS': -1,}
	cscore = {'RR': 'r', 'PP': 'r', 'SS': 'r', \
	          'PR': 'b', 'RS': 'b', 'SP': 'b', \
	          'RP': 'c', 'SR': 'c', 'PS': 'c',}
	beat = {'P': 'S', 'S': 'R', 'R': 'P'}
	cede = {'P': 'R', 'S': 'P', 'R': 'S'}
	rps = ['R', 'P', 'S']
	wlt = {1:0,-1:1,0:2}
	
	def counter_prob(probs):
		weighted_list = []
		for h in rps:
			weighted = 0
			for p in probs.keys():
				points = score[h+p]
				prob = probs[p]
				weighted += points * prob
			weighted_list.append((h, weighted))

		return max(weighted_list, key=operator.itemgetter(1))[0]

	played_probs = defaultdict(lambda: 1)
	dna_probs = [defaultdict(lambda: defaultdict(lambda: 1)) for i in range(18)]
	
	wlt_probs = [defaultdict(lambda: 1) for i in range(9)]

	answers = [{'c': 1, 'b': 1, 'r': 1} for i in range(12)]
	
	patterndict = [defaultdict(str) for i in range(6)]
	
	consec_strat_usage = [[0]*6,[0]*6,[0]*6] #consecutive strategy usage
	consec_strat_candy = [[],   [],   []   ] #consecutive strategy candidates

	output = random.choice(rps)
	histories = ["","",""]
	dna = ["" for i in range(12)]

	sc = 0
	strats = [[] for i in range(3)] 
else:
	prev_sc = sc

	sc = score[output + inp]
	for j in range(3):
		prev_strats = strats[j][:]
		for i, c in enumerate(consec_strat_candy[j]):
			if c == inp:
				consec_strat_usage[j][i] += 1
			else:
				consec_strat_usage[j][i] = 0
		m = max(consec_strat_usage[j])
		strats[j] = [i for i, c in enumerate(consec_strat_candy[j]) if consec_strat_usage[j][i] == m]
		
		for s1 in prev_strats:
			for s2 in strats[j]:
				wlt_probs[j*3+wlt[prev_sc]][chr(s1)+chr(s2)] += 1
				
		if dna[2*j+0] and dna[2*j+1]:
			answers[2*j+0][cscore[inp+dna[2*j+0]]] += 1
			answers[2*j+1][cscore[inp+dna[2*j+1]]] += 1	
		if dna[2*j+6] and dna[2*j+7]:
			answers[2*j+6][cscore[inp+dna[2*j+6]]] += 1
			answers[2*j+7][cscore[inp+dna[2*j+7]]] += 1

		for length in range(min(10, len(histories[j])), 0, -2):
			pattern = patterndict[2*j][histories[j][-length:]]
			if pattern:
				for length2 in range(min(10, len(pattern)), 0, -2):
					patterndict[2*j+1][pattern[-length2:]] += output + inp
			patterndict[2*j][histories[j][-length:]] += output + inp
	played_probs[inp] += 1
	dna_probs[0][dna[0]][inp] +=1
	dna_probs[1][dna[1]][inp] +=1
	dna_probs[2][dna[1]+dna[0]][inp] +=1
	dna_probs[9][dna[6]][inp] +=1
	dna_probs[10][dna[6]][inp] +=1
	dna_probs[11][dna[7]+dna[6]][inp] +=1

	histories[0] += output + inp
	histories[1] += inp
	histories[2] += output
	
	dna = ["" for i in range(12)]
	for j in range(3):
		for length in range(min(10, len(histories[j])), 0, -2):
			pattern = patterndict[2*j][histories[j][-length:]]
			if pattern != "":
				dna[2*j+1] = pattern[-2]
				dna[2*j+0]  = pattern[-1]
				for length2 in range(min(10, len(pattern)), 0, -2):
					pattern2 = patterndict[2*j+1][pattern[-length2:]]
					if pattern2 != "":
						dna[2*j+7] = pattern2[-2]
						dna[2*j+6] = pattern2[-1]
						break
				break

	probs = {}
	for hand in rps:
		probs[hand] = played_probs[hand]
		
	for j in range(3):
		if dna[j*2] and dna[j*2+1]:
			for hand in rps:
				probs[hand] *= dna_probs[j*3+0][dna[j*2+0]][hand] * \
				               dna_probs[j*3+1][dna[j*2+1]][hand] * \
							   dna_probs[j*3+2][dna[j*2+1]+dna[j*2+0]][hand]
				probs[hand] *= answers[j*2+0][cscore[hand+dna[j*2+0]]] * \
				               answers[j*2+1][cscore[hand+dna[j*2+1]]]
			consec_strat_candy[j] = [dna[j*2+0], beat[dna[j*2+0]], cede[dna[j*2+0]],\
			                         dna[j*2+1], beat[dna[j*2+1]], cede[dna[j*2+1]]]
			strats_for_hand = {'R': [], 'P': [], 'S': []}
			for i, c in enumerate(consec_strat_candy[j]):
				strats_for_hand[c].append(i)
			pr = wlt_probs[wlt[sc]+3*j]
			for hand in rps:
				for s1 in strats[j]:
					for s2 in strats_for_hand[hand]:
						probs[hand] *= pr[chr(s1)+chr(s2)]
		else:
			consec_strat_candy[j] = []
	for j in range(3):
		if dna[j*2+6] and dna[j*2+7]:
			for hand in rps:
				probs[hand] *= dna_probs[j*3+9][dna[j*2+6]][hand] * \
				               dna_probs[j*3+10][dna[j*2+7]][hand] * \
							   dna_probs[j*3+11][dna[j*2+7]+dna[j*2+6]][hand]
				probs[hand] *= answers[j*2+6][cscore[hand+dna[j*2+6]]] * \
				               answers[j*2+7][cscore[hand+dna[j*2+7]]]

	output = counter_prob(probs)
"""
class Bot:
    """Basic bot class to wrap bot functions"""
    def __init__(self, name, code=None):
        """
name should be a unique identifier and must be a readable
filename if code is not specified
"""
        self.name = name
        if code is None:
            self.load_code()
        else:
            self.code = code

        self.reset()

    def __eq__(self, other):
        return self.name == other.name

    def get_move(self, inp):
        """Get the next move for the bot given input
input must be "R", "P", "S" or ""
"""
        if self._code is None:
            self.compile_code()

        self.scope["inp"] = inp
        exec(self._code, self.scope)
        self.output = self.scope["output"]
        return self.output

    def compile_code(self):
        self._code = compile(self.code, '<string>', 'exec')

    def reset(self):
        """Resets bot for another round.  This must be called before trying
to pass the bot between workers, or you may see obscure errors from failures
to pickle the bots scope dictionary."""
        self.scope = dict()

        # this will hold compiled code, but it apparently can't be
        # pickled? so we'll have to do it later.  XXX check into this
        self._code = None

    def load_code(self):
        """Load bot code from the file specified by the name attribute"""
        f = open(self.name, "r")
        self.code = f.read()
        f.close()

rps = Bot("markov", code=c)
# main player script logic
paths = [0, 1, 3, 1, 0, 6, 0, 10, 0, 6]
switch_path = [0,1,6,7,8,9,10]
switch = False
current = 0
last_op = ""
# DO NOT CHANGE BELOW ----------------------------
for line in fileinput.input():
    if first_line:
        game = game_API.Game(json.loads(line))
        first_line = False
        continue
    game.update(json.loads(line))
# DO NOT CHANGE ABOVE ---------------------------

    # code in this block will be executed each turn of the game

    me = game.get_self()

    if me.location == 0 and paths[2] == 3 and me.speed >= 2:
        paths.pop(2)
        paths.pop(2)
    
    if me.location == me.destination: # check if we have moved this turn
        # Stay on 0 if close
        if me.location == 0 and game.get_monster(0).respawn_counter <= 10:
            current = current
        else:
            current = (current + 1) % len(paths) if (not game.has_monster(paths[current]) or game.get_monster(paths[current]).dead == True) else current
        """elif me.health < 15:
            destination_node = game.shortest_path(me.location, 0)[0][0]
            current = 0"""
        destination_node = paths[current] 

    else:
        destination_node = me.destination

    if me.location == game.get_opponent().location:
        #destination_node = me.location
        chosen_stance = fromrps(rps.get_move(torps(last_op)))
        last_op = game.get_opponent().stance
    elif game.has_monster(me.location):
        # if there's a monster at my location, choose the stance that damages that monster
        chosen_stance = get_winning_stance(game.get_monster(me.location).stance)
    else:
        # otherwise, pick a random stance
        chosen_stance = stances[random.randint(0, 2)]

    # submit your decision for the turn (This function should be called exactly once per turn)
    game.submit_decision(destination_node, chosen_stance)


